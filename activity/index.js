const http = require ("http");

let items = [

	{
		name: "Iphone X",
		price: 30000,
		isActive: true
	},
	{
		name: "Samsung Galaxy S21",
		price: 51000,
		isActive: true
	},
	{
		name: "Razer Blackshark VX2",
		price: 2800,
		isActive: false
	}
]
const port = 8000;

const server = http.createServer((req, res) => {

    if (req.url === "/" && req.method === "GET")
    {
        res.writeHead(200,{'Content-Type' : 'text/plain'})
        res.end('This route is for checking GET method ')
    }

	else if(req.url === "/items" && req.method === "GET")
    {
		res.writeHead(200, {'Content-Type' : 'application / json'})
        res.end(JSON.stringify(items));
	}

    else if (req.url === "/items" && req.method === "POST")
    {
        let requestBody = "";

        req.on('data', (data) => {

            requestBody += data;

        })

        req.on('end', () =>{
            console.log(requestBody);

            requestBody = JSON.parse(requestBody)

            let newItem = {
                name : requestBody.name,
                price : requestBody.price,
                isActive : requestBody.isActive
            }

            items.push (newItem);
            console.log(items)

            res.writeHead(200, {'Content-Type' : 'application/json'})
            res.end(JSON.stringify(items))
        })
    }

/*STRETCH GOAL*/
    else if (req.url === "/items" && req.method === "DELETE")
    {

      

        res.writeHead(200, {'Content-Type' : 'application/json'})
        res.end(JSON.stringify(items))

        items.pop();
    
    }
   
});
server.listen(port);
console.log(`Server is now accessible at localhost ${port}`)
