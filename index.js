//Node.js Routing with HTTP Methods

//CRUD Operations       HTTP METHOD
    // C - create         POST
    // R - read           GET
    // U - update         UPDATE / PATCH
    // D - delete         DELETE


const http = require ("http");
//mock data for users and courses
let users = [
    {
        username: "PeterParker",
        email: "peterParker@mail.com",
        password: "peterNoWayHome"
    },
    {
        username: "Tony3000",
        email: "starkIndustries@gmail.com",
        password: "ironManWillBeBack"
    }
];
let courses = [
    {
        name : "Math 103",
        price : 2500,
        isActive : true,
    },
    {
        name : "Biology 201",
        price : 2500,
        isActive : true,
    }
];

//SERVER CONNECTION
http.createServer((req, res) => {

    if (req.url === "/" && req.method === "GET")
    {
        res.writeHead(200,{'Content-Type' : 'text/plain'})
        res.end('This route is for checking GET method ')
    }
    else if(req.url === "/" && req.method === "POST")
    {
        res.writeHead(200,{'Content-Type' : 'text/plain'})
        res.end('This route is for checking POST method ')
    }
    else if(req.url === "/" && req.method === "PUT")
    {
        res.writeHead(200,{'Content-Type' : 'text/plain'})
        res.end('This route is for checking PUT method ')
    }
    else if(req.url === "/" && req.method === "DELETE")
    {
        res.writeHead(200,{'Content-Type' : 'text/plain'})
        res.end('This route is for checking DELETE method ')
    }
    else if (req.url === "/users" && req.method === "GET")
    {
        //Change the value of Content-Type header if we are passing json as our server response: 'application/json'
        res.writeHead(200, {'Content-Type' :'application/json'})

        //we cannot pass other data type as a response except  for strings
        //to be able to pass the array of users, first we stringify the array of JSON
        res.end(JSON.stringify(users));
    }
    else if (req.url === "/courses" && req.method ==="GET")
    {
        res.writeHead(200, {'Content-Type' : 'application/json'})
        res.end(JSON.stringify(courses));
    }
    else if (req.url === "/users" && req.method === "POST")
    {
        let requestBody = "";

        //Recieving data from clinet to nodejs server requires 2 steps:

        //data step - this part will read the stream of data from our client and process the incoming data into  the requestBody variable
        req.on('data', (data) => {
            console.log(data)

            console.log(data)

            requestBody += data;
        })

        //end step - will run once or after the request has been completely sent from our client
        req.on('end', () =>{
            console.log(requestBody);

            requestBody = JSON.parse(requestBody)

            let newUser = {
                username : requestBody.username,
                email : requestBody.email,
                password : requestBody.password
            }

            users.push (newUser);
            console.log(users)

            res.writeHead(200, {'Content-Type' : 'application/json'})
            res.end(JSON.stringify(users))
        })
    }

    else if (req.url === "/courses" && req.method === "POST")
    {
        let requestBody = "";

        req.on('data', (data) => {
            console.log(data)

            requestBody += data;
        })
        req.on('end', () =>{
            console.log(requestBody);

            requestBody = JSON.parse(requestBody)

            let newCourse = {
                name : requestBody.name,
                price : requestBody.price,
                isActive : requestBody.isActive
            }

            courses.push (newCourse);
           

            res.writeHead(200, {'Content-Type' : 'application/json'})
            res.end(JSON.stringify(courses))
        })
    }

}).listen(4000);
console.log(`Server is now accessible at localhost 4000`)